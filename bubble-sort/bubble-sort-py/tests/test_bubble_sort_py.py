from bubble_sort_py import __version__
import bubble_sort_py as bl

def test_version():
  assert __version__ == '0.1.0'

def test_uni_array():
  for i in range(100):
    assert bl.sort([i]) == [i]

def test_already_cresc():
  array = [i for i in range(15)]
  assert bl.sort(array) == array

def test_decresc():
  array = [i for i in range(15,-1,-1)]
  array.sort()
  assert bl.sort(array) == array

def test_part_cresc():
  array = [i for i in range(15)]
  for i in range(30,14,-1):
    array.append(i)
  assert bl.sort(array) == array

def test_part_decresc():
  array = [i for i in range(30,14,-1)]
  for i in range(15):
    array.append(i)
  assert bl.sort(array) == array

def test_already_cresc_neg():
  array = [i for i in range(-15,0)]
  assert bl.sort(array) == array

def test_decresc_neg():
  array = [i for i in range(0,-15,-1)]
  array.sort()
  assert bl.sort(array) == array

def test_part_cresc():
  array = [i for i in range(15)]
  for i in range(-1,-14,-1):
    array.append(i)
  assert bl.sort(array) == array

def test_part_decresc():
  array = [i for i in range(-1,-14,-1)]
  for i in range(15):
    array.append(i)
  assert bl.sort(array) == array

