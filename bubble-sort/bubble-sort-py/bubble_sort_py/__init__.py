__version__ = '0.1.0'

def sort(array):
  print(array)
  if len(array) < 2:
    return array

  size = len(array)
  swaped = True

  while(swaped):
    swaped = False
    for i in range(size-1):
      if array[i] > array[i+1]:
        array[i], array[i+1] = array[i+1], array[i]
        swaped = True
  print(array)
  return array
