#include <gtest/gtest.h>
#include <bits/stdc++.h>
#include "bubble-sort.h"

TEST(sort,uni_array) {
  for(int i = 0; i < 10; i++) {
    int list[] = {i};
    int aux[] = {i};
    sort(list,1);
    EXPECT_EQ(aux[0],list[0]);
  }
}

TEST(sort, cresc) {
  int list[15];
  int aux[15];

  for(int i = 0; i < 15; i++) {
    list[i] = i;
    aux[i] = i;
  }

  sort(list,15);

  for(int i = 0; i < 15; i++)
    EXPECT_EQ(aux[i],list[i]);
}

TEST(sort, decresc) {
  auto size = 15u;
  int list[size];
  int aux[size];

  for(auto i = 0u; i < size; i++) {
    list[i] = size-i;
    aux[i] = size-i;
  }

  std::sort(aux,aux+size);
  sort(list,size);

  for(auto i = 0u; i < size; i++)
    EXPECT_EQ(aux[i],list[i]) << "value [" << i << "] expected: "<<  aux[i] << " real: "<< list[i];
}

TEST(sort, part_cresc) {
  int list[21];
  int aux[21];
  std::size_t size = 21, aux_size = 21;

  for(auto i = 0u; i < size; i++) {
    if(i < 11) {
      list[i] = i;
      aux[i] = i;
    } else {
      list[i] = aux_size;
      aux[i] = aux_size;
      aux_size--;
    }
  }

  std::sort(aux,aux+21);
  sort(list,21);

  for(int i = 0; i < 21; i++)
    EXPECT_EQ(aux[i],list[i]);
}

TEST(sort, part_decresc) {
  std::size_t size = 21;
  int list[size];
  int aux[size];

  for(auto i = 0u; i < size; i++) {
    if(i < size/2) {
      list[i] = size-i;
      aux[i] = list[i];
    } else {
      list[i] = i;
      aux[i] = list[i];
    }
  }


  for(auto i = 0u; i < size; i++) {
    std::cout << "[" << i << "]: " << "list[" << i << "] = " << list[i];
    std::cout << " aux[" << i << "] = " << aux[i] << "\n";
  }


  std::sort(aux,aux+size);
  sort(list,size);

  for(auto i = 0u; i < size; i++) {
    std::cout << "[" << i << "]: " << "list[" << i << "] = " << list[i];
    std::cout << " aux[" << i << "] = " << aux[i] << "\n";
  }

  for(auto i = 0u; i < size; i++)
    EXPECT_EQ(aux[i],list[i]) << "value [" << i << "] expected: "<<  aux[i] << " real: "<< list[i];
}


int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
