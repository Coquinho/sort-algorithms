#include "bubble-sort.h"

inline void swap(int list[], int a, int b) {
  auto aux = list[a];
  list[a] = list[b];
  list[b] = aux;
}

void sort(int list[], std::size_t size) {
  if(size < 2)
    return;

  bool swapped = true;
  while(swapped) {
    swapped = false;
    for(std::size_t i = 0u; i < size-1; i++) {
      if(list[i] > list[i+1]){
        swap(list,i,i+1);
        swapped = true;
      }
    }
  }
  return;
}
