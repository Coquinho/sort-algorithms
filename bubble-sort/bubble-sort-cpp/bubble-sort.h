#ifndef BUBBLE_SORT_H
#define BUBBLE_SORT_H

#include <cstddef>

inline void swap(int list[], int a, int b);
void sort(int list[], std::size_t size);
#endif
